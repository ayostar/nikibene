import React from 'react';

const Hero = () => {
  return (
    <section className="hero hero-bg is-primary is-fullheight">
      <div className="hero-body">
        <div className="container has-text-centered">
          <h1 className="title is-size-1">Ahoj, jmenuji se Nikola Benešová</h1>
          <h2 className="subtitle">a jsem grafická designérka.</h2>
        </div>
      </div>
    </section>
  );
};

export default Hero;
