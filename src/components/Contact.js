import React, { Component } from 'react';

class Contact extends Component {
  render() {
    return (
      <section className="hero is-fullheight" style={{ marginTop: '80px' }}>
        <div className="container">
          <article className="message is-large z-depth-2" style={{ marginTop: '5px' }}>
            <div className="message-header">
              <h1 className="is-1">KONTAKT</h1>
            </div>
            <div className="message-body">
              <form className="form" style={{ width: '480px', margin: '0 auto' }}>
                <div className="field">
                  <label className="label is-medium">Vaše jméno:</label>
                  <div className="control has-icons-left has-icons-right">
                    <input className="input is-medium" type="text" placeholder="Jméno" />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <label className="label is-medium">Váš email:</label>
                  <div className="control has-icons-left has-icons-right">
                    <input className="input is-medium" type="email" placeholder="Email" />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <label className="label is-medium">Předmět:</label>
                  <div className="control has-icons-left has-icons-right">
                    <input className="input is-medium" type="text" placeholder="Předmět" />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <label className="label is-medium">Předmět:</label>
                  <div className="control">
                    <textarea className="textarea is-medium" placeholder="Obsah Vaší zprávy:" />
                  </div>
                </div>
                <div className="field">
                  <p className="control">
                    <button className="button is-large is-primary">Poslat zprávu</button>
                  </p>
                </div>
              </form>
            </div>
          </article>
        </div>
      </section>
    );
  }
}

export default Contact;
