import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './Header';
import Hero from './Hero';
import Elle from './Elle';
import Posters from './Posters';
import Wintic from './Wintic';
import Contact from './Contact';
import Footer from './Footer';

/* TODO: check possible bug with reload! */
/* TODO: remove all inline styles! */
/* TODO: finish Kontakt component and hook it up to either firebase function or php */
/* TODO: add links to social medias */
/* TODO: refactor projects */
/* TODO: think about adding admin section */

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route component={ScrollToTop} />
          <Route path="*" component={Header} />
          <Route exact path="/" component={Hero} />
          <Switch>
            <Route path="/elle" component={Elle} />
            <Route path="/posters" component={Posters} />
            <Route path="/wintic" component={Wintic} />
            <Route path="/contact" component={Contact} />
          </Switch>
          <Route path="*" component={Footer} />
        </div>
      </Router>
    );
  }
}

const ScrollToTop = () => {
  window.scrollTo(0, 0);
  return null;
};

export default App;
