import React, { Component } from 'react';
import Lightbox from 'lightbox-react';
import image1 from '../img/POSTERS/1.png';
import image2 from '../img/POSTERS/2.png';
import image3 from '../img/POSTERS/3.png';
import image4 from '../img/POSTERS/4.png';
import image5 from '../img/POSTERS/5.png';
import image6 from '../img/POSTERS/6.png';
import image7 from '../img/POSTERS/7.png';
import image8 from '../img/POSTERS/8.png';
import image9 from '../img/POSTERS/9.png';

const images = [image1, image2, image3, image4, image5, image6, image7, image8, image9];

class Posters extends Component {
  constructor() {
    super();
    this.state = {
      photoIndex: 0,
      isOpen: false
    };
  }
  openLightbox(event, index) {
    event.preventDefault();
    this.setState({
      photoIndex: index,
      isOpen: true
    });
  }
  render() {
    const { photoIndex, isOpen } = this.state;
    return (
      <section className="hero is-primary is-fullheight">
        <div className="hero-body">
          <div className="container">
            <section style={{ marginTop: '80px' }}>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image1} alt="" onClick={e => this.openLightbox(e, 0)} />
                </div>
                <div className="column is-one-third">
                  <img src={image2} alt="" onClick={e => this.openLightbox(e, 1)} />
                </div>
                <div className="column is-one-third">
                  <img src={image3} alt="" onClick={e => this.openLightbox(e, 2)} />
                </div>
              </div>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image4} alt="" onClick={e => this.openLightbox(e, 3)} />
                </div>
                <div className="column is-one-third">
                  <img src={image5} alt="" onClick={e => this.openLightbox(e, 4)} />
                </div>
                <div className="column is-one-third">
                  <img src={image6} alt="" onClick={e => this.openLightbox(e, 5)} />
                </div>
              </div>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image7} alt="" onClick={e => this.openLightbox(e, 6)} />
                </div>
                <div className="column is-one-third">
                  <img src={image8} alt="" onClick={e => this.openLightbox(e, 7)} />
                </div>
                <div className="column is-one-third">
                  <img src={image9} alt="" onClick={e => this.openLightbox(e, 8)} />
                </div>
              </div>
            </section>
          </div>
        </div>
        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length
              })}
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length
              })}
          />
        )}
      </section>
    );
  }
}

export default Posters;
