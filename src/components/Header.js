import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import logo from '../img/logo.png';

class Header extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false
    };
  }
  handleMenu = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };
  componentWillReceiveProps() {
    this.handleMenu();
  }
  render() {
    const { isOpen } = this.state;
    return (
      <nav className="navbar is-fixed-top has-shadow is-primary">
        <div className="container">
          <div className="navbar-brand">
            <Link className="navbar-item" to="/">
              <img src={logo} alt="Home" />
            </Link>
            <button
              className={
                isOpen
                  ? 'button navbar-burger is-primary is-active'
                  : 'button navbar-burger is-primary'
              }
              onClick={this.handleMenu}>
              <span />
              <span />
              <span />
            </button>
          </div>
          <div className={isOpen ? 'navbar-menu is-active' : 'navbar-menu'}>
            <div className="navbar-end">
              <NavLink className="navbar-item" to="/elle" activeClassName="is-active">
                ELLE
              </NavLink>
              <NavLink className="navbar-item" to="/posters" activeClassName="is-active">
                POSTERS
              </NavLink>
              <NavLink className="navbar-item" to="/wintic" activeClassName="is-active">
                WINTIC
              </NavLink>
              <NavLink className="navbar-item" to="/contact" activeClassName="is-active">
                KONTAKT
              </NavLink>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;
