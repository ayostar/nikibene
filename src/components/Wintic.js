import React, { Component } from 'react';
import Lightbox from 'lightbox-react';
import banner from '../img/WINTIC/banner.jpg';
import image1 from '../img/WINTIC/1.jpg';
import image2 from '../img/WINTIC/2.jpg';
import image3 from '../img/WINTIC/3.jpg';
import image4 from '../img/WINTIC/4.jpg';
import image5 from '../img/WINTIC/5.jpg';
import image6 from '../img/WINTIC/6.jpg';
import image7 from '../img/WINTIC/7.jpg';
import image8 from '../img/WINTIC/8.jpg';
import image9 from '../img/WINTIC/9.jpg';
import image10 from '../img/WINTIC/10.jpg';
import image11 from '../img/WINTIC/11.jpg';
import image12 from '../img/WINTIC/12.jpg';
import image13 from '../img/WINTIC/13.jpg';
import image14 from '../img/WINTIC/14.jpg';

const images = [
  banner,
  image1,
  image2,
  image3,
  image4,
  image5,
  image6,
  image7,
  image8,
  image9,
  image10,
  image11,
  image12,
  image13,
  image14
];

class Wintic extends Component {
  constructor() {
    super();
    this.state = {
      photoIndex: 0,
      isOpen: false
    };
  }
  openLightbox(event, index) {
    event.preventDefault();
    this.setState({
      photoIndex: index,
      isOpen: true
    });
  }
  render() {
    const { photoIndex, isOpen } = this.state;
    return (
      <section className="hero is-primary is-fullheight">
        <div className="hero-body">
          <div className="container">
            <section style={{ marginTop: '80px' }}>
              <div className="columns">
                <div className="column has-text-centered">
                  <img src={banner} alt="" onClick={e => this.openLightbox(e, 0)} />
                </div>
              </div>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image1} alt="" onClick={e => this.openLightbox(e, 1)} />
                </div>
                <div className="column is-one-third">
                  <img src={image2} alt="" onClick={e => this.openLightbox(e, 2)} />
                </div>
                <div className="column is-one-third">
                  <img src={image3} alt="" onClick={e => this.openLightbox(e, 3)} />
                </div>
              </div>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image4} alt="" onClick={e => this.openLightbox(e, 4)} />
                </div>
                <div className="column is-one-third">
                  <img src={image5} alt="" onClick={e => this.openLightbox(e, 5)} />
                </div>
                <div className="column is-one-third">
                  <img src={image6} alt="" onClick={e => this.openLightbox(e, 6)} />
                </div>
              </div>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image7} alt="" onClick={e => this.openLightbox(e, 7)} />
                </div>
                <div className="column is-one-third">
                  <img src={image8} alt="" onClick={e => this.openLightbox(e, 8)} />
                </div>
                <div className="column is-one-third">
                  <img src={image9} alt="" onClick={e => this.openLightbox(e, 9)} />
                </div>
              </div>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image10} alt="" onClick={e => this.openLightbox(e, 10)} />
                </div>
                <div className="column is-one-third">
                  <img src={image11} alt="" onClick={e => this.openLightbox(e, 11)} />
                </div>
                <div className="column is-one-third">
                  <img src={image12} alt="" onClick={e => this.openLightbox(e, 12)} />
                </div>
              </div>
              <div className="columns">
                <div className="column is-one-third">
                  <img src={image13} alt="" onClick={e => this.openLightbox(e, 13)} />
                </div>
                <div className="column is-one-third">
                  <img src={image14} alt="" onClick={e => this.openLightbox(e, 14)} />
                </div>
              </div>
            </section>
          </div>
        </div>
        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length
              })}
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length
              })}
          />
        )}
      </section>
    );
  }
}

export default Wintic;
