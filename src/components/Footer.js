import React from 'react';

const Footer = () => {
  return (
    <footer className="footer" style={{ padding: '2rem 1.5rem 2rem' }}>
      <div className="container">
        <div className="columns is-vcentered">
          <div className="column ">Copyright &copy; Nikola Benešová 2017</div>
          <div className="column has-text-right">
            <span className="icon is-large has-text-primary">
              <i className="fa fa-3x fa-facebook-square " />
            </span>
            <span className="icon is-large has-text-primary">
              <i className="fa fa-3x fa-twitter " />
            </span>
            <span className="icon is-large has-text-primary">
              <i className="fa fa-3x fa-instagram " />
            </span>
            <span className="icon is-large has-text-primary">
              <i className="fa fa-3x fa-google-plus-square " />
            </span>
            <span className="icon is-large has-text-primary">
              <i className="fa fa-3x fa-pinterest " />
            </span>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
